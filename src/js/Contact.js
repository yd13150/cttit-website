import React, { Component } from 'react'
import '../css/Contact.css'
import { Form, Button, Input } from 'element-react'
import {getData, uploadData} from './Tools'
class Contact extends Component{
  constructor() {
    super()
    this.state = {
      userInfo: {
        username: '',
        email: '',
        theme: '',
        content: ''
      },
      customInfo: {},
      submitEnable: false
    }
  }
  componentWillMount () {
    getData('customInfo', '/api/custom/getList').then(data => {
      let customInfo = {}
      data.forEach(item => {
        customInfo[item.key] = item.content
      })
      this.setState({customInfo})
    })
  }
  componentDidMount () {
    let BMap = window.BMap
    let map = new BMap.Map('map', {
      enableMapClick: false
    })
    let point = new BMap.Point(116.379288, 39.960366)
    map.centerAndZoom(point, 15)
    // map.enableScrollWheelZoom()
    let marker = new BMap.Marker(point)
    map.addOverlay(marker)
    let label = new BMap.Label("北京工业设计创意产业基地",{offset:new BMap.Size(-117,-45)});
    marker.setLabel(label);
  }
  onChange(key, value) {
    let userInfo = {...this.state.userInfo}
    userInfo[key] = value
    let submitEnable = true
    Object.keys(userInfo).forEach(key => {
      submitEnable &= userInfo[key].length > 0
    })
    this.setState({
      userInfo,
      submitEnable
    });
  }
  submit () {
    uploadData('/api/leave-message/save', this.state.userInfo).then(res => {
      if(res.status === 200) {
        this.setState({
          userInfo: {
            username: '',
            email: '',
            theme: '',
            content: ''
          },
          submitEnable: false
        })
        alert('保存成功')
      }
    })
  }
  render () {
    return (<div className="contact">
      <img src={require('../img/contact.png')} className="topImg pulse animated" alt=""/>
      <div className="contactDetail container shadow">
        <div id="map" className="map" />
        <div className="contactWay">
          <div className="our">
            <p className="title"><img src={require('../img/phone.png')} alt=""/>电话：</p>
            <p className="value">{this.state.customInfo.phone}</p>
            <p className="title"><img src={require('../img/afterSale.png')} alt=""/>售后电话：</p>
            <p className="value">{this.state.customInfo.serverphone}</p>
            <p className="title"><img src={require('../img/fax.png')} alt=""/>传真：</p>
            <p className="value">{this.state.customInfo.fax}</p>
            <p className="title"><img src={require('../img/email.png')} alt=""/>邮箱：</p>
            <p className="value">{this.state.customInfo.email}</p>
            <p className="title"><img src={require('../img/address.png')} alt=""/>地址：</p>
            <p className="value">{this.state.customInfo.address}</p>
          </div>
          <div className="your">
            <Form ref="form">
              <Input placeholder="姓名" value={this.state.userInfo.username} onChange={this.onChange.bind(this, 'username')}/>
              <Input placeholder="邮箱" value={this.state.userInfo.email} onChange={this.onChange.bind(this, 'email')}/>
              <Input placeholder="主题" value={this.state.userInfo.theme} onChange={this.onChange.bind(this, 'theme')}/>
              <Input placeholder="信息" type="textarea" resize="none" rows={8} value={this.state.userInfo.content} onChange={this.onChange.bind(this, 'content')}/>
              <Button disabled={!this.state.submitEnable} onClick={this.submit.bind(this)} style={{marginTop: '20px', width: '100px'}} type="primary">提交</Button>
            </Form>
          </div>
        </div>
      </div>
    </div>)
  }
}
export default Contact

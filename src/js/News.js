import React, { Component } from 'react'
import {Route} from 'react-router-dom'
import NewsList from './NewsList'
import NewsDetail from './NewsDetail'
import '../css/News.css'

class News extends Component{
  constructor() {
    super()
    this.state = {type: 0}
  }
  componentWillMount () {
    let type = this.props.location.query ? this.props.location.query.type : -1
    if (type !== -1) this.setState({type})
  }
  componentWillReceiveProps (nextProps) {
    let type = nextProps.location.query ? nextProps.location.query.type : -1
    if (type !== -1) this.setState({type})
  }
  render () {
    let imgSrc = require(`../img/${['allNews', 'companyNews', 'industryNews'][this.state.type]}.png`)
    return (<div className="news">
      <img src={imgSrc} className="topImg pulse animated" alt=""/>
      <Route exact path='/news' component={props => {
        let newProps = Object.assign({type: this.state.type}, props)
        return <NewsList {...newProps}/>
      }}/>
      <Route path='/news/detail' component={NewsDetail}/>
    </div>)
  }
}
export default News

import React, { Component } from 'react'
import {Route} from 'react-router-dom'
import '../css/App.css'
import Header from './Header'
import Footer from './Footer'
import Home from './Home'
import About from './About'
import News from './News'
import Product from './Product'
import Solution from './Solution'
import Successful from './Successful'
import Contact from './Contact'
import {throttle} from './Tools'
class App extends Component {
  componentWillMount () {
    window.addEventListener('scroll', () => {throttle.call(this, this.showToTopBtn)})
    window.addEventListener('unload', () => {localStorage.clear()})
  }
  showToTopBtn () {
    let showToTopBtn = window.pageYOffset > document.body.offsetHeight * 0.2
    this.refs.toTop.style.display = showToTopBtn ? 'block' : 'none'
  }
  scrollToTop () {
    let target = 0
    let timer = null
    let leader = window.pageYOffset
    timer = setInterval(function () {
      leader = Math.floor(leader + (target - leader) / 10)
      window.scrollTo(0, leader)
      if (leader === target) {
        clearInterval(timer)
      }
    }, 10)
  }
  render () {
    return (<div className="app">
      <Header />
      <div className="content">
        <Route exact path='/' component={Home} />
        <Route path='/about' component={About} />
        <Route path='/news' component={News} />
        <Route path='/product' component={Product} />
        <Route path='/solution' component={Solution} />
        <Route path='/successful' component={Successful} />
        <Route path='/contact' component={Contact} />
      </div>
      <Footer/>
      <div className="toTop" ref="toTop" onClick={this.scrollToTop.bind(this)}/>
    </div>)
  }
}
export default App

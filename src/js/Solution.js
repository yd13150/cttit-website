import React from 'react'
import Case from './Case'
export default () => {
  let options = {
    name: 'solutions',
    title: '解决案例',
    banner: require('../img/solution.png'),
    type: 2,
    icon: require('../img/solutionIcon.png')
  }
  return <Case options={options}/>
}

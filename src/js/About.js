import React, { Component } from 'react'
import '../css/About.css'
import ModuleTitle from './ModuleTitle'
import {Carousel, Dialog} from 'element-react'
import {getData} from './Tools'
class About extends Component {
  constructor () {
    super()
    this.state = {
      teamList: [],
      honorList: [],
      preview: false
    }
    this.cultureArr = [{
      bgImg: require('../img/culture_1.png'),
      icon: require('../img/culture_icon1.png'),
      title: '企业愿景',
      detail: ['壹卡行车，智行天下', '驾驶更安全，家庭更幸福']
    }, {
      bgImg: require('../img/culture_2.png'),
      icon: require('../img/culture_icon2.png'),
      title: '核心价值观',
      detail: ['专注、人本、共赢、幸福']
    }, {
      bgImg: require('../img/culture_3.png'),
      icon: require('../img/culture_icon3.png'),
      title: '核心价值观',
      detail: ['创新、务实、拼搏、奉献']
    }]
  }
  componentDidMount () {
    getData('teamList', '/api/personnel/getList').then(teamList => {
      this.setState({teamList})
    })
    getData('honor', '/api/honor/getList', true).then(honor => {
      this.setState({
        honorList: honor.data,
        honorExplain: honor.content
      })
    })
    this.goAnchor(this.props)
  }
  componentWillReceiveProps (nextProps) {
    this.goAnchor(nextProps)
  }
  goAnchor (props) {
    if (!props.location.query) {
      window.scroll(0, 0)
      return
    }
    let anchor = props.location.query.anchor
    let anchorEl = document.querySelector('.' + anchor)
    window.scroll(0, anchorEl.offsetTop - 145)
  }
  previewImage (index) {
    this.refs.carousel.setActiveItem(index)
    this.setState({preview: true, index})
  }
  render () {
    return (<div className="about">
      <img src={require('../img/aboutUs.png')} className="topImg pulse animated" alt=""/>
      <div className="companyIntro module container">
        <ModuleTitle title="公司简介"/>
        <div className="intro">
          <div className="detail animated bounceInLeft">
            <p><span style={{fontWeight: 'bolder'}}>壹卡行</span>科技有限公司是以营运车辆相关数据为核心的，以运输安全</p>
            <p>综合解决方案为目的的，集营运车辆安全的<span className="keywords">产品设计，</span></p>
            <p><span className="keywords">研发，生产，销售，平台运营于一体</span>的营运</p>
            <p>车辆综合安全数据服务公司</p>
          </div>
          <img src={require('../img/companyIntro_bg.png')} alt=""/>
        </div>
      </div>
      <div className="companyCulture module container">
        <ModuleTitle title="公司文化"/>
        <div className="culture">
          {
            this.cultureArr.map((item, index) => {
              return (<div key={index} className="item" style={{backgroundImage: `url(${item.bgImg})`}}>
                <img src={item.icon} alt=""/>
                <p className="title">{item.title}</p>
                {
                  item.detail.map((explain, i) => {
                    return <p className="explain" key={i}>{explain}</p>
                  })
                }
              </div>)
            })
          }
        </div>
      </div>
      <div className="teamIntro module">
        <ModuleTitle title="团队介绍"/>
        <div className="teamItem">
          {
            this.state.teamList.map((item, index) => {
              return (<div key={index} className="item container">
                <img className="portrait" src={item.photo} alt=""/>
                <div className="detail">
                  <p style={{fontSize: '24px', fontWeight: 'bold'}}>{item.name}</p>
                  <p className="post" style={{marginLeft: '50px', fontSize: '20px', padding: '15px 0'}}>{item.job}</p>
                  <ul>
                    {
                      item.introduce.split('$').map((intro, i) => {
                        return (<li key={i}>{intro}</li>)
                      })
                    }
                  </ul>
                </div>
              </div>)
            })
          }
        </div>
      </div>
      <div className="aptitudeHonor module container">
        <ModuleTitle title="资质荣誉"/>
        <p>{this.state.honorExplain}</p>
        <ul className="honor clearfix">
          {
            this.state.honorList.map((item, index) => {
              return (<li key={index} onClick={this.previewImage.bind(this, index)}>
                <img src={item.image} alt=""/>
                <p>{item.honorName}</p>
              </li>)
            })
          }
        </ul>
      </div>
      <div className="developCurse module container">
        <ModuleTitle title="发展历程"/>
        <img src={require('../img/course.png')} alt=""/>
      </div>
      <Dialog visible={ this.state.preview } top="1vh" onCancel={ () => this.setState({ preview: false }) }>
        <Dialog.Body>
          <Carousel ref="carousel" indicatorPosition="none" autoplay={false}>
            {
              this.state.honorList.map((item, index) => {
                return (
                  <Carousel.Item key={index}>
                    <img src={item.image} alt=""/>
                  </Carousel.Item>
                )
              })
            }
          </Carousel>
        </Dialog.Body>
      </Dialog>
    </div>)
  }
}
export default About

import React, { Component } from 'react'
import { Pagination, Button } from 'element-react'
import { getData } from './Tools'

class NewsList extends Component {
  constructor() {
    super()
    this.state = {
      type: 0,
      total: 0,
      dataList: []
    }
  }
  componentWillMount () {
    this.getDataList(1)
  }
  getDataList (currentPage) {
    let type = this.props.type
    let url = `/api/article/${!type ? 'allNews' : 'getList'}?type=${type === 2 ? 10 : type}&current=${currentPage}&size=5`
    let itemName = ['allNews', 'companyNews', 'industryNews'][type]
    getData(itemName, url).then(data => {
      this.setState({
        total: data.total,
        dataList: data.records
      })
    })
  }
  newsDetail (index) {
    this.props.history.push({pathname: '/news/detail', state: {index, dataList: this.state.dataList}})
  }
  render () {
    return ( <div className="newsList container">
      <div>
        {
          this.state.dataList.map((item, index) => {
            return (<div className="newsItem" key={index}>
              <div className="top">
                <img className="picture" src={item.picture} alt=""/>
                <div className="right">
                  <p style={{fontSize: '16px', color: '#444', fontWeight: 'Bold'}}>{item.title}</p>
                  <p style={{fontSize: '14px', color: '#666', lineHeight: '30px'}}>{item.description}</p>
                  <p style={{fontSize: '12px', color: '#999'}}>{item.showDate}</p>
                </div>
              </div>
              <p className="bottom"><Button onClick={this.newsDetail.bind(this, index)}>查看详情</Button></p>
            </div>)
          })
        }
      </div>
      <Pagination onCurrentChange={this.getDataList.bind(this)} layout="prev, pager, next" pageSize={5} total={this.state.total}/>
    </div>)
  }
}

export default NewsList

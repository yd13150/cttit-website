import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { Dropdown } from 'element-react'
import '../css/Header.css'

class Header extends Component {
  constructor() {
    super()
    this.state = {
      currentIndex: 0
    }
    this.menuList = [{path: '/', name: '首页'},
      {path: '/about', name: '关于我们'},
      {path: '/news', name: '新闻中心'},
      {path: '/product', name: '产品介绍'},
      {path: '/solution', name: '解决方案'},
      {path: '/successful', name: '成功案例'},
      {path: '/contact', name: '联系我们'}]
  }
  componentWillMount () {
    this.setCurrentIndex(this.props)
  }
  componentWillReceiveProps (nextProps) {
    this.setCurrentIndex(nextProps)
  }
  setCurrentIndex (props) {
    let pathname = props.location.pathname
    for (let i = this.menuList.length - 1; i >= 0; i--) {
      let menu = this.menuList[i]
      if (pathname.includes(menu.path)) {
        this.setState({currentIndex: i})
        return
      }
    }
  }
  toAbout(command) {
    this.props.history.push({pathname: '/about', query: {anchor: command}})
  }
  toProduct(command) {
    this.props.history.push({pathname: '/product', query: {type: command}})
  }
  toNews(command) {
    this.props.history.push({pathname: '/news', query: {type: parseInt(command)}})
  }
  render() {
    let liArr = []
    for (let i = 0; i < this.menuList.length; i++) {
      let item = this.menuList[i]
      if (item.path === '/about') {
        liArr.push(<li key={i} className={this.state.currentIndex === i ? 'active' : ''}>
          <Dropdown onCommand={this.toAbout.bind(this)} menu={(
            <Dropdown.Menu>
              <Dropdown.Item command="companyIntro">公司简介</Dropdown.Item>
              <Dropdown.Item command="companyCulture">公司文化</Dropdown.Item>
              <Dropdown.Item command="teamIntro">团队介绍</Dropdown.Item>
              <Dropdown.Item command="aptitudeHonor">资质荣誉</Dropdown.Item>
              <Dropdown.Item command="developCurse">发展历程</Dropdown.Item>
            </Dropdown.Menu>
          )}>
            <Link to={item.path}>{item.name}</Link>
          </Dropdown>
        </li>)
      } else if (item.path === '/product') {
        liArr.push(<li key={i} className={this.state.currentIndex === i ? 'active' : ''}>
          <Dropdown onCommand={this.toProduct.bind(this)} menu={(
            <Dropdown.Menu>
              <Dropdown.Item command="0">软件产品</Dropdown.Item>
              <Dropdown.Item command="1">硬件产品</Dropdown.Item>
            </Dropdown.Menu>
          )}>
            <Link to={item.path}>{item.name}</Link>
          </Dropdown>
        </li>)
      } else if (item.path === '/news') {
        liArr.push(<li key={i} className={this.state.currentIndex === i ? 'active' : ''}>
          <Dropdown onCommand={this.toNews.bind(this)} menu={(
            <Dropdown.Menu>
              <Dropdown.Item command="1">企业新闻</Dropdown.Item>
              <Dropdown.Item command="2">行业新闻</Dropdown.Item>
            </Dropdown.Menu>
          )}>
            <Link to={{pathname: item.path, query: {type: 0}}}>{item.name}</Link>
          </Dropdown>
        </li>)
      } else {
        liArr.push(<li key={i} className={this.state.currentIndex === i ? 'active' : ''}>
          <Link exact="true" to={item.path}>{item.name}</Link>
        </li>)
      }
    }
    return (<div className="header">
      <img src={require('../img/header-logo.png')} alt=""/>
      <ul className="menu animated fadeInDown">
        {liArr}
      </ul>
    </div>)
  }
}
export default withRouter(Header)

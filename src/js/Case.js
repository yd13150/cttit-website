import React, { Component } from 'react'
import '../css/Case.css'
import axios from 'axios'
import {getData} from './Tools'

class Case extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dataList: [],
      currentIndex: 0,
      particulars: ''
    }
  }
  componentWillMount () {
    this.getDataList(this.props.options)
  }
  componentWillReceiveProps (nextProps) {
    this.getDataList(nextProps.options)
  }
  getDataList (options) {
    let url = `/api/article/getList?type=${options.type}&current=1&size=10`
    getData(options.name, url).then(data => {
      this.setState({dataList: data.records})
      this.selectCase(0)
    })
  }
  selectCase (index) {
    setTimeout(() => {
      this.setState({currentIndex: index})
      let data = this.state.dataList[index]
      if (!data) return
      let url = `/api/article/getOne?id=${data.id}`
      axios.get(url).then( res=> {
        if (res.status === 200) {
          let particulars = res.data.data.content
          this.setState({particulars})
        }
      })
    }, 0)
  }
  render() {
    return (<div className="case">
      <img src={this.props.options.banner} className="topImg pulse animated" alt=""/>
      <div className="caseContent container shadow clearfix">
        <div className="nav">
          <p className="title"><span className="caseIcon" style={{backgroundImage: `url(${this.props.options.icon})`}}/>{this.props.options.title}</p>
          <ul>
            {
              this.state.dataList.map((item, index) => (
                <li className={this.state.currentIndex === index ? 'active' : ''}
                    onClick={this.selectCase.bind(this, index)} key={index}>
                  {item.title}
                  <span className="arrow">></span>
                </li>
              ))
            }
          </ul>
        </div>
        <div className="caseDetail" dangerouslySetInnerHTML={{__html: this.state.particulars}}/>
      </div>
    </div>)
  }
}

export default Case

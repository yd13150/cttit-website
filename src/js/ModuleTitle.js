import React, { Component } from 'react'
import '../css/ModuleTitle.css'
class ModuleTitle extends Component {
  componentDidMount () {
    if (this.props.themeColor) {
      let color = this.props.themeColor
      this.refs.title.style.color = color
      this.refs.left.style.backgroundColor = color
      this.refs.right.style.backgroundColor = color
    }
  }
  render () {
    return (<p className="moduleTitle" ref="title">
      <span className="left" ref="left"/>
      {this.props.title}
      <span className="right" ref="right"/>
    </p>)
  }
}
export default ModuleTitle

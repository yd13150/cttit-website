import React, { Component } from 'react'
import { Carousel, Button } from 'element-react'
import '../css/Home.css'
import {throttle, getData} from './Tools'
import ModuleTitle from './ModuleTitle'

class Home extends  Component {
  constructor () {
    super()
    this.state = {
      bannerHeight: '',
      bannerArr: [],
      selectTeamIndex: 0,
      newsArr: [],
      teamArr: [{
        img: require('../img/home_team_com.png'),
        selImg: require('../img/home_team_com_sel.png'),
        title: '公司介绍',
        detailImg: require('../img/home_team_1.png')
      }, {
        img: require('../img/home_team_dev.png'),
        selImg: require('../img/home_team_dev_sel.png'),
        title: '研发团队',
        detailImg: require('../img/home_team_2.png')
      }, {
        img: require('../img/home_team_pro.png'),
        selImg: require('../img/home_team_pro_sel.png'),
        title: '生产团队',
        detailImg: require('../img/home_team_3.png')
      }, {
        img: require('../img/home_team_ope.png'),
        selImg: require('../img/home_team_ope_sel.png'),
        title: '运维团队',
        detailImg: require('../img/home_team_4.png')
      }]
    }
    this.resizeFunc = () => {throttle.call(this, this.resize)}
  }
  componentWillMount () {
    getData('bannerArr', '/api/swipe/getList').then(bannerArr => {
      this.setState({bannerArr})
    })
    getData('newsArr', '/api/article/getList?type=1&current=1&size=3').then(newsArr => {
      this.setState({newsArr: newsArr.records})
    })
    getData('customInfo', '/api/custom/getList').then(data => {
      let customInfo = {}
      data.forEach(item => {
        customInfo[item.key] = item.content
      })
      this.state.teamArr.forEach((item, index) => {
        let key = ['companyDesc', 'teamDesc', 'productDesc', 'ywDesc'][index]
        item.description = customInfo[key]
      })
      this.setState({teamArr: this.state.teamArr})
    })
    window.addEventListener('resize', this.resizeFunc)
  }
  componentDidMount() {
    this.resize()
  }
  componentWillUnmount () {
    window.removeEventListener('resize', this.resizeFunc)
  }
  resize () {
    this.refs.decision.style.height = 660 * (this.refs.home.offsetWidth / 1920) + 'px'
    this.refs.decision.style.paddingTop = 60 * (this.refs.home.offsetWidth / 1920) + 'px'
    this.refs.contact.style.bottom = 60 * (this.refs.home.offsetWidth / 1920) + 'px'
    let bannerHeight = 930 * (this.refs.home.offsetWidth / 1920) + 'px'
    this.setState({bannerHeight})
  }
  selectTeam (index) {
    this.setState({selectTeamIndex: index})
  }
  toOtherPage (path) {
    this.props.history.push(path)
  }
  newsDetail (index) {
    this.props.history.push({pathname: '/news/detail', state: {index, dataList: this.state.newsArr}})
  }
  render () {
    let partnerList = []
    for (let i = 1; i <= 12; i++) {
      partnerList.push(<img src={require(`../img/partner${i}.png`)} key={i}/>)
    }
    return (<div className="home" ref="home">
      <div className="banner pulse animated">
        <Carousel trigger="click" height={this.state.bannerHeight}>
          {
            this.state.bannerArr.map((item, index) => {
              return (
                <Carousel.Item key={index}>
                  <img src={item.url} alt=""/>
                </Carousel.Item>
              )
            })
          }
        </Carousel>
      </div>
      <div className="product module container">
        <ModuleTitle title="我们的产品，是您安全的保障"/>
        <div className="clearfix">
          <div className="item">
            <p>AI人工智能算法车载终端</p>
            <div>
              <p>汽车驾驶行为分析仪</p>
              <p>汽车行驶记录仪</p>
              <p>汽车防撞预警分析ADAS</p>
            </div>
          </div>
          <div className="item">
            <p>大数据车联网信息化管理云平台</p>
            <div>
              <p>天王星车辆运营平台</p>
              <p>联网联控平台</p>
              <p>第三方检测平台</p>
              <p>第三方监控平台</p>
            </div>
          </div>
        </div>
      </div>
      <div className="team module">
        <ModuleTitle title="我们的团队，是您坚实的后盾"/>
        <div className="teamContent">
          <div className="item">
            {
              this.state.teamArr.map((item, index) => {
                return (<p key={index} onClick={this.selectTeam.bind(this, index)} className={this.state.selectTeamIndex === index ? 'selected' : ''}>
                  <img src={this.state.selectTeamIndex === index ? item.selImg : item.img} alt=""/>
                  <span>{item.title}</span>
                </p>)
              })
            }
          </div>
          <div className="detail">
            <p className={'detailContent' + this.state.selectTeamIndex}>
              {this.state.teamArr[this.state.selectTeamIndex].description}
            </p>
            <img src={this.state.teamArr[this.state.selectTeamIndex].detailImg} className={'img' + this.state.selectTeamIndex} alt=""/>
          </div>
        </div>
      </div>
      <div className="companyNews">
        <ModuleTitle title="公司动态"/>
        <div className="newsItem">
          {
            this.state.newsArr.map((item, index) => {
              return (<p key={index} onClick={this.newsDetail.bind(this, index)} className="item">
                <img src={item.picture} alt=""/>
                <span>{item.title}</span>
              </p>)
            })
          }
        </div>
        <p style={{textAlign: 'center'}}><Button type="primary" onClick={this.toOtherPage.bind(this, '/news')}>查看更多</Button></p>
      </div>
      <div className="partner module container">
        <ModuleTitle title="我们的合作伙伴"/>
        <div className="partnerItem">
          {partnerList}
        </div>
      </div>
      <div className="decision module" ref="decision">
        <ModuleTitle title="我们会是您正确的选择" themeColor="white"/>
        <p style={{textAlign: 'center'}} ref="contact"><Button onClick={this.toOtherPage.bind(this, '/contact')}>联系我们</Button></p>
      </div>
    </div>)
  }
}
export default Home

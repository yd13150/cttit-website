import React, { Component } from 'react'
import '../css/Footer.css'
import { withRouter } from 'react-router-dom'

class Footer extends  Component {
  constructor() {
    super()
    this.state = {isHome: false}
  }
  componentWillMount () {
    this.setState({isHome: this.props.location.pathname === '/'})
  }
  componentWillReceiveProps (nextProps) {
    this.setState({isHome: nextProps.location.pathname === '/'})
  }
  render () {
    return (<div className={`footer ${this.state.isHome ? 'home' : ''}`}>
      <div className='companyInfo'>
        <div className="logo">
          <img src={require(`../img/${this.state.isHome ? 'foot_home_logo' : 'foot_logo'}.png`)} alt=""/>
        </div>
        <div className="detail">
          <p>北京壹卡行科技有限公司</p>
          <p>公司地址：北京市西城区新街口外大街28号普天德胜B座402</p>
          <p>Email：ykx@ykx2014.com</p>
          <p>网址：http://www.ykx2014.com</p>
          <p>传真：+86 010-62156925</p>
        </div>
        <div className="qrcode">
          <img src={require('../img/weichat.png')} alt=""/>
        </div>
      </div>
      <div className="copyright">
        京ICP备17063469号 COPYRIGHT © 2014 CTTIT
      </div>
    </div>)
  }
}
export default withRouter(Footer)

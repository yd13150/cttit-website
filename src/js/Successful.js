import React from 'react'
import Case from './Case'
export default () => {
  let options = {
    name: 'successfulCases',
    title: '成功案例',
    banner: require('../img/successful.png'),
    type: 4,
    icon: require('../img/successfulIcon.png')
  }
  return <Case options={options}/>
}

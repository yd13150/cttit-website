import React from 'react'
import Case from './Case'
export default props => {
  let type = props.location.query ? parseInt(props.location.query.type) : 0
  let options = {
    name: type ? 'hardwareList' : 'softwareList',
    title: type ? '硬件产品' : '软件产品',
    banner: require('../img/' + (type ? 'hardware.png' : 'software.png')),
    type: type ? 5 : 9,
    icon: require('../img/productIcon.png')
  }
  return (<Case options={options}/>)
}

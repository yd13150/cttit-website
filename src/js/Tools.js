import axios from 'axios'
import qs from 'qs'
function throttle(fn, ...args) {
  let now = new Date()
  if (!fn.time) fn.time = now
  if (now - fn.time >= 200) {
    clearTimeout(fn.timer)
    fn.call(this, ...args)
    fn.time = now
  } else {
    clearTimeout(fn.timer)
    fn.timer = setTimeout(() => {
      fn.call(this, ...args)
      fn.time = new Date()
    }, 200)
  }
}

function getData(itemName, url, needContent) {
  return new Promise(resolve => {
    if (!url.includes('current=') || url.includes('current=1')) {
      let data = localStorage.getItem(itemName)
      if (data) {
        resolve(JSON.parse(data))
        return
      }
    }
    axios.get(url).then((res) => {
      if (res.status === 200) {
        let responseData = res.data
        let data = needContent ? responseData : responseData.data
        if (!url.includes('current=') || url.includes('current=1')) {
          localStorage.setItem(itemName, JSON.stringify(data))
        }
        resolve(data)
      }
    })
  })
}

function uploadData(url, data, method='post') {
  return axios({
    url: url,
    method,
    data: qs.stringify(data)
  })
}
export {throttle, getData, uploadData}

import React, { Component } from 'react'
import { Button } from 'element-react'
import axios from 'axios'

class NewsDetail extends Component {
  constructor () {
    super()
    this.state = {
      content: '',
      index: ''
    }
  }
  componentWillMount () {
    this.getDetail(this.props.location.state.index)
  }
  getDetail (index) {
    let dataList = this.props.location.state.dataList
    this.setState({
      index,
      isFirst: index === 0,
      isLast: index === dataList.length - 1
    })
    axios.get(`/api/article/getOne?id=${dataList[index].id}`).then(res => {
      if (res.status === 200) {
        this.setState({content: res.data.data.content})
        window.scrollTo(0, 300)
      }
    })
  }
  render () {
    return (<div className="newsDetail">
      <div className="newsContent container">
        <div dangerouslySetInnerHTML={{__html: this.state.content}} />
        <p className="changeBtn">
          <Button disabled={this.state.isFirst} onClick={this.getDetail.bind(this, this.state.index - 1)}>上一篇</Button>
          <Button disabled={this.state.isLast} onClick={this.getDetail.bind(this, this.state.index + 1)}>下一篇</Button>
        </p>
      </div>
    </div>)
  }
}
export default NewsDetail
